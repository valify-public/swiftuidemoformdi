//
//  HomeView.swift
//  SwiftUIDemo
//
//  Created by Mina Atef on 15/12/2022.
//

import SwiftUI
import IDVMDIOCR
import IDVMDILiveness
struct HomeView: View {
    @State var isOpenView = false
    var body: some View {
        NavigationView {
            VStack {
                NavigationLink(destination: TestControllerView(), isActive: $isOpenView) {
                    EmptyView()
                }
                
                Button(action: {
                    self.isOpenView = true
                }){
                    Text("Tap here")
                }
            }
        }
        
    }
}

struct TestControllerView : UIViewControllerRepresentable {
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
    }
    
    func makeUIViewController(context: Context) -> some UIViewController {
        
        guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "TestViewController") as? TestViewController else {
            fatalError("ViewController not implemented in storyboard")
        }
        
        return viewController
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
