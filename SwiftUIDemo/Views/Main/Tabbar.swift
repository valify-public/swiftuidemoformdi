//
//  Tabbar.swift
//  SwiftUIDemo
//
//  Created by Mina Atef on 15/12/2022.
//

import SwiftUI
import IDVMDIOCR
struct Tabbar: View {
    var body: some View {
        TabView {
            
            HomeView()
                .tabItem{
                    Label("Home", systemImage: "house")
                }
            
        }
    }
}

struct Tabbar_Previews: PreviewProvider {
    static var previews: some View {
        Tabbar()
    }
}
