//
//  ViewController.swift
//  TestMDIOCR
//
//  Created by Mina Atef on 05/06/2022.
//

import UIKit
import IDVMDIOCR
import IDVMDILiveness

struct Credentials{
    let baseURL = ""
    let userName = ""
    let password = ""
    let clientID = ""
    let clientSecret = ""
    let bundleKey = ""
    var token :String = ""
}

class TestViewController: UIViewController {
    //:MARK: Varialbels
    var credentials = Credentials()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getToken {[weak self] token in
            guard let self = self,let token = token else {
                print("Invalid Token")
                return
            }
            self.credentials.token = token
            DispatchQueue.main.async {
                let builder = OCRBuilder()
                builder.dataValidation(validate: true)
                    .documentVerification(verify: true)
                    .language(language: "en")
                    .bundleKey(self.credentials.bundleKey)
                    .baseUrl(self.credentials.baseURL)
                    .token(token)
                    .addHeaders(headers: ["key":"value", "key2":"value2"])
                    .start(vc: self, ocrDelegate: self)
            }
        }
    }
    
    func callLivness(frontImage:Data){
        // required settings
        IDVLiveness.sharedInstance.delegate = self
        guard let frontImage = UIImage(data: frontImage) else {return}
        let settings = IDVLivenessBuilder()
            .set(bundle: credentials.bundleKey)
            .set(token: credentials.token)
            .set(baseURL: credentials.baseURL)
            .set(customFacialRecognitionImage: frontImage)
            .set(numberOfInstructions: 5)
        
        // add optional settings here
            .build()
        // build settings
        let IDVRun = IDVLivenessFlow.init(withSettings: settings, fromViewController: self)
        do {
            try IDVRun.run()
        } catch {}
    }
    
}
//MARK: Token Creation
extension TestViewController{
    func getToken( completion: @escaping (_ token: String?) -> ()){
        let url = "\(credentials.baseURL)/api/o/token/"
        var request  = URLRequest(url: URL(string: url)!)
        request.httpMethod = "POST"
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let body = "username=\(credentials.userName)&password=\(credentials.password)&client_id=\(credentials.clientID)&client_secret=\(credentials.clientSecret)&grant_type=password".data(using:String.Encoding.ascii, allowLossyConversion: false)
        request.httpBody = body
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            do {
                
                guard let resdata = data else {
                    return
                }
                
                guard let jsonResponse = try JSONSerialization.jsonObject(with: (resdata), options: JSONSerialization.ReadingOptions()) as? NSDictionary else {
                    return
                }
                print(jsonResponse)
                guard let token = jsonResponse.value(forKey: "access_token") as? String else {
                    return
                }
                completion(token)
            } catch _ {
                print("not good JSON formatted response")
            }
        }
        task.resume()
    }
}
//MARK: OCR Response
extension TestViewController:IDVOCRDelegate{
    func onOCRDataExtractionLog(log: IDVDataExtractionLog) {
        print("🏳️Logs:: Side: \(log.side?.rawValue ?? ""),Position: \(log.position?.rawValue ?? ""),Index: \(log.captureIndex ?? 0),reason: \(log.reason?.rawValue ?? ""),status:\(log.status?.rawValue ?? ""),timestamp: \(log.timestamp ?? Date())")
    }
    
    func onOCRGenericLog(log: IDVGenericLog) {
        print("🚩Logs:: Service: \(log.service?.rawValue ?? ""), Status \(log.status?.rawValue ?? ""), SDKVersion: \(log.sdkVersion ?? ""), Time Stamp: \(log.timestamp ?? Date())")
    }
    
    func onOCRResult(result: IDVOCRResponse) {
        switch result{
        case .Success(data: let data):
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {[weak self] in
                guard let frontImage = data?.captures?.nationalIdFront,let self = self else {
                    print("Invalid Image")
                    return
                }
                self.callLivness(frontImage:frontImage)
            }
        case .BuilderError(code: let code, message: let message):
            print("message:",message," code:" ,code)
        case .ServiceFailure(code: let code, message: let message, data: let data,let contactSupport):
            print("message:",message," code:" ,code," Contact Support: ",contactSupport ?? false)
        case .Exit(step: let step, data: let data):
            print("step:",step)
        default:
            break
        }
    }
}

//MARK: Liveness & Facematch Response
extension TestViewController:IDVLivenessDelegate{
    func onLivenessGenericLog(log: IDVLivenessGenericLog) {
        print("🚩Logs:: Service: \(log.service?.rawValue ?? ""), Status \(log.status?.rawValue ?? ""), SDKVersion: \(log.sdkVersion ?? ""),status:\(log.status?.rawValue ?? ""), Time Stamp: \(log.timestamp ?? Date())")
    }
    
    func onLivenessActionsLog(log: IDVLivenessActionsLog) {
        print("🏳️Logs:: Instruction: \(log.instruction?.rawValue ?? ""), reason: \(log.reason?.rawValue ?? ""),status:\(log.status?.rawValue ?? "")")
    }
    
    func livenessDidFinishProcess(_ IDVLivenessResponse: IDVLivenessResponse) {
        switch IDVLivenessResponse{
        case .success(livenessResult: let livenessResult):
            print("Liveness Success:\(livenessResult.livenessSuccess ?? false) Facematch Success: \(livenessResult.facematchSuccess ?? false)")
        case .error(error: let error):
           print("\(error.errorCode ?? 0)")
        case .userExited:
           print("User Exit")
        default:
            break
        }
    }
}

