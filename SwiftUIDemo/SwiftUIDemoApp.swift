//
//  SwiftUIDemoApp.swift
//  SwiftUIDemo
//
//  Created by Mina Atef on 13/12/2022.
//

import SwiftUI

@main
struct SwiftUIDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
