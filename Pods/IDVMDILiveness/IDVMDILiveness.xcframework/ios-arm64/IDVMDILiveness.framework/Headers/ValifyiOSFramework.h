//
//  IDViOSFramework.h
//  IDViOSFramework
//
//  Created by Sayed Obaid on 4/9/19.
//  Copyright © 2019 IDV. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for IDViOSFramework.
FOUNDATION_EXPORT double IDViOSFrameworkVersionNumber;

//! Project version string for IDViOSFramework.
FOUNDATION_EXPORT const unsigned char IDViOSFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IDViOSFramework/PublicHeader.h>


